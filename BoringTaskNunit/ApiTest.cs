﻿using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;


namespace BoringTaskNunit2
{
    public class TestsApi
    {

        static readonly HttpClient client = new HttpClient();

        [Test]
        public async System.Threading.Tasks.Task TestSearchAsync()
        {
            HttpResponseMessage response = await client.GetAsync("https://jsonplaceholder.typicode.com/users");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            var usersJson = JsonConvert.DeserializeObject<JArray>(responseBody);
            Assert.GreaterOrEqual(usersJson.Count, 10);
         
        }

    }
}