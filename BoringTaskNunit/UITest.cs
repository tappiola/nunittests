using System;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace BoringTaskNunit
{
    public class TestsSelenium
    {

        private IWebDriver driver;
        public string homeURL;

        [SetUp]
        public void Setup()
        {
            homeURL = "https://www.gov.uk/";
            driver = new ChromeDriver();
        }

        [Test]
        public void TestSearch()
        {
            driver.Navigate().GoToUrl(homeURL);
            driver.FindElement(By.CssSelector(".gem-c-search input")).SendKeys("visa" + Keys.Enter);
            var links = driver.FindElements(By.CssSelector(".gem-c-document-list__item-title"));
            Assert.GreaterOrEqual(links.Count, 5);
            links.Take(5).ToList().ForEach(l => Assert.IsTrue(l.Text.ToLower().Contains("visa")));
            // alternative
            foreach (var link in links.Take(5))
            {
                Assert.IsTrue(link.Text.ToLower().Contains("visa"));
            }
        }

        [TearDown]
        public void Teardown()
        {
            driver.Quit();
        }
    }
}